function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
        if (letter.length !== 1) {
            return undefined;
        }
        //Dito nag ccheck tayo kung single letter lng sya pag hindi ireturn natin sya ng undefined
    
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                result++;
            }
        }
        // Dito nag loop tau sa sentence at ihalintulad natin sya sa letter kapag may nag match saknya mag increment lang then ireturn natin si result
        return result;
    }
    


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    text = text.toLowerCase();
    // Una gumamit muna ako ng convertion ng text lowercase or uppercase para lng madisregard yung casing nya
    for (let i = 0; i < text.length; i++) {
    //Dito gumamit ako ng loop para macheck ko kung madaming beses syang lumalabas pag hindi return ko lng yung false
        if (text.indexOf(text[i]) !== text.lastIndexOf(text[i])) {
            return false;
        }
    }
    return true;
    // if hindi nmn nag retrun ng false meaning yung text sa for loop ko naka true pa din 
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    if (age < 13) {
        return undefined;
    //Dito nagcheck ako if less than 13 sya mag return lang ako ng "undefined"
      } else if (age >= 13 && age <= 21 || age >= 60) {
        return (Math.round(price * 0.8 * 100) / 100).toFixed(2);
    //Dito nmn sa else if ko nag check ako if less than equal to 13 ka or 21 or greater than equal to 60 ka (senior citizen)  mag return ng discount price na naka round off pero dapat two decimal places lang kaya gumamit ako ng math.rount method 
      } else {
        return (Math.round(price * 100) / 100).toFixed(2);
    // itong naman kapag greather than 21 at less 60 years old mag return lng ako ng round off value
    // so ung price na 0.8 sa senior sa else if na nauna ay 20% percent discount gumamit din ako ng math.round and nag multipy ako ng 100 para ma move ung decimal point nya to two place 
    //Then gumamit din ako ng toFixed method para masure ko n yung return nya is string with 2 decimal
      }
    }

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let hotCategories = [];
    for (let i = 0; i < items.length; i++) {
      if (items[i].stocks === 0 && !hotCategories.includes(items[i].category)) {
        hotCategories.push(items[i].category);
      }
    }
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let flyingVoters = [];
    // yung una kong ginawa dito nag create muna ako ng let ng flyingVoters as array para ma store ko duon 
    //Then gumamit ako ng loop para macheck if yung voter ko sa A ay bumoto din kay B
    // if hindi nmn iaadd ko lng sya sa array
    for (let i = 0; i < candidateA.length; i++) {
      if (candidateB.includes(candidateA[i]) && !flyingVoters.includes(candidateA[i])) {
        flyingVoters.push(candidateA[i]);
      }
    }
    //then irereturn ko lng yung sa array kun sino bumoto kay A at kay V
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};